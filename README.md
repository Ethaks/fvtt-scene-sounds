# Scene Sounds

Scene Sounds allows setting a Background Playlist in a Scene's configuration window, whose sounds will be played whenever the scene is currently viewed.
The modules respects a Playlist's mode, and can either play sounds sequentially, or all of a Playlist's sounds at the same time.

## Installation

To install the module navigate to Foundry's _Add-on Modules_ tab in the Setup menu and paste the following link in the **Install Module** dialog:

[https://gitlab.com/Ethaks/fvtt-scene-sounds/-/raw/latest/src/module.json](https://gitlab.com/Ethaks/fvtt-scene-sounds/-/raw/latest/src/module.json)

The module can also be installed manually by downloading a zip archive from the Releases Page and extracting it to Foundry's `Data/modules/scene-sounds` directory.

## Development

### Prerequisites

In order to build this module, recent versions of `node` and `npm` are
required. Most likely using `yarn` also works but only `npm` is officially
supported. We recommend using the latest lts version of `node`, which is
`v14.15.5` at the time of writing.

You also need to install the project's dependencies. To do so, run

```
npm install
```

### Building

You can build the project by running

```
npm run build
```

Alternatively, you can run

```
npm run build:watch
```

to watch for changes and automatically build as necessary.

### Linking the built project to Foundry VTT

In order to provide a fluent development experience, it is recommended to link
the built module to your local Foundry VTT installation's data folder. In
order to do so, first add a file called `foundryconfig.json` to the project root
with the following content:

```
{
  "dataPath": "/absolute/path/to/your/FoundryVTT/Data"
}
```

(if you are using Windows, make sure to use `\` as a path separator instead of `/`)

Then run

```
npm run link-project
```

On Windows, creating symlinks requires administrator privileges so unfortunately
you need to run the above command in an administrator terminal for it to work.

## Legal

The software component of this system is distributed under the EUPL v. 1.2.
The terms of the [Foundry Virtual Tabletop End User License Agreement](https://foundryvtt.com/article/license/) apply.
