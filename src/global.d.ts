export {};

declare global {
  interface FlagConfig {
    Scene: {
      "scene-sounds"?: {
        playlist?: string;
      };
    };
  }

  interface PlaylistSound {
    data: {
      path: string;
      repeat: boolean;
    };
  }
  // @ts-expect-error Missing types
  class Playlist extends ClientDocumentMixin(foundry.documents.BasePlaylist) {
    sounds: Collection<PlaylistSound>;
  }
}
