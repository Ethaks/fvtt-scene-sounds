/**
 * Author: Ethaks
 * Software License: EUPL-1.2
 */

/** The module's name */
const MODULE_NAME = "scene-sounds";
/** The localisation prefix */
const LOCALISATION_PREFIX = "SceneSounds";

/** An array containing all sounds whose playing was triggered by this module */
let playingSceneSounds: Sound[] = [];

// Check for a configured playlist when a scene is loaded, and play its sounds
Hooks.on("canvasInit", async (canvas) => {
  // Stop all sounds triggered by previous scenes
  playingSceneSounds.forEach((s) => s.stop());
  playingSceneSounds = [];

  const playlistId = canvas.scene?.getFlag(MODULE_NAME, "playlist");
  if (playlistId) {
    const playlist = getGame().playlists?.get(playlistId);
    if (!playlist)
      return console.error(`Scene Sounds | Playlist [${playlistId}] cannot be found for Scene ${canvas.scene?.name}`);

    const mode = playlist.data.mode;
    if (mode === foundry.CONST.PLAYLIST_MODES["SIMULTANEOUS"]) {
      // Play all sounds at once, respecting loop settings
      playlist.sounds
        .map((s: PlaylistSound) => ({ src: s.data.path, loop: s.data.repeat ?? false }))
        .forEach(async (s: AudioHelper.PlayData) => {
          const sound = await AudioHelper.play(s, false);
          playingSceneSounds.push(sound);
        });
    } else if (mode === foundry.CONST.PLAYLIST_MODES["SEQUENTIAL"]) {
      // Play first sound if any, register callback to play next one
      const sounds = playlist.sounds.map((s: PlaylistSound) => ({ src: s.data.path }));
      if (sounds.length) {
        const sound = await AudioHelper.play(sounds[0]);
        playingSceneSounds.push(sound);
        sound.on("end", playNextSound(sounds, sound));
      }
    }
  }
});

/**
 * Returns an inner function that loops through an array of sound data objects, plays the next one,
 * and registers a callback to continue this cycle
 */
const playNextSound = (sounds: AudioHelper.PlayData[], sound: Sound) => async () => {
  const index = sounds.findIndex((s) => s.src === sound.src);
  const next = sounds[(index + 1) % sounds.length];
  const newSound = await AudioHelper.play(next);
  playingSceneSounds.forEach((s) => s.stop());
  playingSceneSounds = [newSound];
  newSound.on("end", playNextSound(sounds, newSound));
};

// Adds a Background Playlist select to a scene's configuration window
Hooks.on("renderSceneConfig", (app: SceneConfig, html: JQuery<HTMLElement>) => {
  const playlistSelect = html.find('select[name="playlist"]')?.first()?.parent();
  const playListChoices = getGame().playlists?.map(
    (list) =>
      `<option value="${list.id}" ${app.object.data.flags[MODULE_NAME]?.playlist === list.id ? "selected" : ""}>${
        list.name
      }</option>`,
  );
  if (playlistSelect) {
    playlistSelect.after(
      `<div class="form-group">
      <label>${localize("BackgroundPlaylist")}</label>
      <select name="flags.${MODULE_NAME}.playlist"><option value></option>${playListChoices}</select>
      <p class="notes">${localize("BackgroundPlaylistNote")}</p>
      </div>`,
    );
    app.setPosition();
  }
});

// Adjust volume of this module's sounds when global interface volume is changed
Hooks.on("globalInterfaceVolumeChanged", (volume: number) => {
  playingSceneSounds.forEach((s) => (s.volume = volume));
});

// Utility functions

/** Returns game if it is initialised, otherwise throws an error */
const getGame = (): Game => {
  if (!(game instanceof Game)) throw new Error("Game not initialised!");
  return game;
};

/** Localises a string, adding this module's localisation scope to allow for shorter parameters */
const localize = (key: string) => getGame().i18n.localize(`${LOCALISATION_PREFIX}.${key}`);
